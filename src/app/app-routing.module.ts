import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TemplateComponent } from './components/template/template.component';

const routes: Routes = [
  { path: 'Formulario39', component: TemplateComponent},
  { path: '**', pathMatch: 'full', redirectTo: 'formulario39'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
